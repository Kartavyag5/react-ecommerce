import React from "react";
import { selectIsLogged } from "../features/loginSlice";
import { useDispatch, useSelector } from "react-redux";


const Cart = ()=>{
  const IsLogged = useSelector(selectIsLogged);
  
  return(
    <div>
    <h1>Cart </h1>
    <p>{IsLogged ? <h5>CartItems</h5>:<h5>please login first</h5>}</p>
    
    </div>
  );
}

export default Cart;