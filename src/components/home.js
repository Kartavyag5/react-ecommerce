import React from "react";
import { Button, Card } from 'antd';

const Home = (props) => {

  function AddToCart(){
    const CartItem = { item:props.items, user:'logged user'}

  }
  const itemsList =props.items.map((item)=>
  <>
  <Card title={item.name} extra={<a href="#">More</a>} style={{ width: 300 }}>
    <h2>{item.name}</h2>
    <h4>{item.price}$</h4>
    <p>{item.description}</p>
    <Button onClick={AddToCart}>Add to Cart</Button>
  </Card>
  
</>
)
  return (
    <>
    <h1>Home Page</h1>
    <h4>Items:</h4>
    <p>{itemsList}</p>
    </>
    );
};
  
export default Home;