import React from "react";
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/layout";
import Home from "./components/home";
import Login from "./components/login";
import Register from "./components/register";
import NoPage from './components/nopage';
import Cart from "./components/cart";

const App = () => {

  const DATA = [
    {id:1, name:'item1', description:'this is item1', price:10},
    {id:2, name:'item2', description:'this is item2', price:20},
    {id:3, name:'item3', description:'this is item3', price:30}
  ];
  
  return(
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home items={DATA} />} />
          <Route path="login" element={<Login />} />
          <Route path="register" element={<Register />} />
          <Route path="cart" element={<Cart />} />
          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;